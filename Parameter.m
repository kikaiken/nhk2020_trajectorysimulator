% 経路の数を指定
num_of_path = 11;

% 経由点・その点での角度を設定(x, y, theta)

via_point_start2gerge = [
    -0.650  0.500;
    -0.650  0.501;
    
    -1.200  5.500;
    
    -2.560  6.200;
    
    -4.075  6.300;
    
    -5.799  6.909
    -5.800  6.910;
];

% ゲルゲ渡し
via_point_gerge_pass = [
    -5.00 8.37;
    -7.00 8.37;
];

% 走行パラメータを設定
velocity_max = 7.0;         %最高速
lateral_acc_max = 5.0;      %ターン時の横G
acc_max = 5.0;              %加速度
angvel_max = 1 * pi;        %最大角速度
angacc_max = 1 * pi;        %角加速度

% 制御周期
CTRL_CYCLE = 0.01;

%機体サイズ
MR1_LENGTH = 0.7;
MR1_WIDTH = 0.7;