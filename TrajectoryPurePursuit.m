% 画像の表示
fig = figure(3);
img = imread('NHK2019_Field.png');  %画像読み込み
image([0, -13.3], [0, 10], img);    %表示
pbaspect([13.3, 10, 1]);            %縦横比を実際と合わせる
%軸反転
ax = gca;
ax.XDir = 'reverse';
hold on;                            %重ね合わせOK
%plot(trajectory(:, 1), trajectory(:, 2));
scatter(trajectory(:, 1), trajectory(:, 2), 5);

% Pure Pursuit
pos = [via_point(1, 1) via_point(1, 2) angle_start];  %現在位置
actual_path = [via_point(1, 1), via_point(1, 2), angle_start];
time_ms = 1;
nearest_index = 1;
reference_index = 1;
while reference_index < trajectory_length
    % 前回の最も近い点の前後10cmの中から、新たに最も近い点を探す
    min_length = 1;
    for i = max(nearest_index - 10, 1) : min(nearest_index + 10, trajectory_length)
        if (hypot((pos(1) - trajectory(i, 1)).^2, (pos(2) - trajectory(i, 2)).^2) < min_length)
            nearest_index = i;
            min_length = hypot((pos(1) - trajectory(i, 1)).^2, (pos(2) - trajectory(i, 2)).^2);
        end
    end
    % 経路上の目標点を決める
    % 速さに比例して目標点を遠くする
    reference_index = nearest_index + round(7 * spd(nearest_index));
    
    % 現在位置から目標点に向かう向きを求める
    diff_x = trajectory(reference_index, 1) - pos(1);
    diff_y = trajectory(reference_index, 2) - pos(2);
    
    % 現在位置更新
    pos(1) = pos(1) + spd(nearest_index) * diff_x / hypot(diff_x, diff_y) * CTRL_CYCLE;
    pos(2) = pos(2) + spd(nearest_index) * diff_y / hypot(diff_x, diff_y) * CTRL_CYCLE;
    pos(3) = pos(3) + angvel(round((pos(3) - angle_start) * 1000) + 2) * CTRL_CYCLE;
    
    % Pure Pursuit走行経路保存
    actual_path(time_ms, 1) = pos(1);
    actual_path(time_ms, 2) = pos(2);
    actual_path(time_ms, 3) = pos(3);
    time_ms = time_ms + 1;
end

% Pure Pursuit走行経路表示
plot(actual_path(:, 1), actual_path(:, 2), 'r');
hold on;

% 角度-時間グラフ
figure(4);
plot(1 : time_ms - 1, actual_path(:, 3));

% 予想タイム表示
[expected_time_ms, ~] = size(actual_path);
expected_time_s = expected_time_ms * CTRL_CYCLE;
print = sprintf("予想タイム = %f秒", expected_time_s);
disp(print);

% MR1通過経路表示
figure(3);
for i = 1 : 100 : expected_time_ms
    x = [actual_path(i,1)-(MR1_WIDTH/2) actual_path(i,1)+(MR1_WIDTH/2) actual_path(i,1)+(MR1_WIDTH/2) actual_path(i,1)-(MR1_WIDTH/2)];
    y = [actual_path(i,2)-(MR1_LENGTH/2) actual_path(i,2)-(MR1_LENGTH/2) actual_path(i,2)+(MR1_LENGTH/2) actual_path(i,2)+(MR1_LENGTH/2)];
    mr1_rectangle = polyshape(x, y);
    mr1_rotate = rotate(mr1_rectangle, actual_path(i, 3) * 180 / pi, [actual_path(i,1) actual_path(i,2)]);
    plot(mr1_rotate, 'FaceColor', 'black', 'FaceAlpha', 0.01)
    drawnow;
    frame = getframe(fig);
    im{i} = frame2im(frame);
end

% gif保存
filename = sprintf('Animation%d.gif', trajectory_num);
for i = 1 : 100 : expected_time_ms
    [A,map] = rgb2ind(im{i},256);
    if i == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0.1);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.1);
    end
end