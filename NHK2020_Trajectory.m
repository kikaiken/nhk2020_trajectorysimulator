clear;

%Clothoid
addpath('./Clothoid4MR1/matlab')

% パラメータ読み込み
Parameter

% フィールド図面描画
DrawField(1);

for path_num = 1 : num_of_path
    if path_num == 1
        via_point = via_point_start2gerge;
    elseif path_num == 2
        via_point = via_point_gerge_pass;
    end
    
    % 経路種類によって違う動作をする処理
    if path_num == 4
        velocity_max = 0.8;
    else
        velocity_max = 5.0;
    end
        
    % クロソイド曲線で経路生成
    [path, path_length] = MakePathClothoid(via_point);
    
    % 経路から曲率マップを生成
    [curvature_map] = MakeCurvatureMap(path, path_length);
    
    % 曲率マップから速度マップを生成
    [velocity_map] = MakeVelocityMap(path_length, curvature_map, velocity_max, lateral_acc_max, acc_max);
    
    % 回転角速度マップを生成
    [angvel_map] = MakeAngVelMap(angvel_max, angacc_max);
    
    % リファレンス点と経路を描画
    figure(1);
    scatter(via_point(:, 1), via_point(:, 2), 'filled');
    plot(path(:, 1), path(:, 2));
    
%    figure(2);
%   subplot(num_of_path, 1, path_num);
%    plot(curvature_map(:));
%    title('譖ｲ邇?縺ｮ邨ｶ蟇ｾ蛟､');
%    xlabel('霍晞屬(cm)');
%    ylabel('譖ｲ邇?');
    
%    figure(3);
%    subplot(num_of_path, 1, path_num);
%    plot(velocity_map(:));
%    title('騾溷ｺｦ繝槭ャ繝?');
%    xlabel('霍晞屬(cm)');
%    ylabel('騾溘＆(m/s)');
    
%    figure(4);
%    subplot(num_of_path, 1, path_num);
%    plot(angvel_map(:));
%    title('隗帝?溷ｺｦ繝槭ャ繝?');
%    xlabel('隗貞ｺｦ(rad)');
%    ylabel('隗帝?溷ｺｦ(rad/s)');
    
    % 移動シミュレーション
    [actual_path, angle_ref, expected_time] = PurePursuit(path, path_length, velocity_map, angvel_map, angle_start_end(1), CTRL_CYCLE, path_num);
    % 経路表示
    DrawField(5);
    plot(actual_path(:, 1), actual_path(:, 2), 'r');
    % Gif保存
    MakeGif(path_num, actual_path, expected_time, MR1_LENGTH, MR1_WIDTH);
    
    % ボタンが押されるまで待つ
    waitforbuttonpress;
end


%----------------------------------------------------------
%   関数
%----------------------------------------------------------
% フィールド図面をグラフに描画
function [] = DrawField(fig_num)
    figure(fig_num);
    img = imread('NHK2020_Field.png');
    image([0, -13.3], [0, 10], img);
    pbaspect([13.3, 10, 1]);  %アスペクト比を現実と合わせる
    %軸反転
    ax = gca;
    ax.XDir = 'reverse';
    hold on;
end

% 繧ｹ繝励Λ繧､繝ｳ繧定ｨ育ｮ励＠縲√＆繧峨↓轤ｹ鄒､繧?1cm縺斐→縺ｮ轤ｹ縺ｫ縺ｪ繧九ｈ縺?縺ｫ髢灘ｼ輔″縺吶ｋ
function [path, path_length] = MakePath(via)
    % 繧ｹ繝励Λ繧､繝ｳ繧定ｨ育ｮ?
    [n_via, ~] = size(via);
    t = 1 : n_via;
    ts = 1 : 1/5000 : n_via;
    xs = spline(t, via(:,1), ts);
    ys = spline(t, via(:,2), ts);
    %figure(1);
    %plot(xs,ys,'r');

    % 轤ｹ髢薙?ｮ霍晞屬繧定ｨ育ｮ?
    [~, n_spline_point] = size(xs);
    dist = zeros(n_spline_point, 1);
    for i = 1 : n_spline_point - 1
        dist(i, 1) = hypot(xs(i+1) - xs(i), ys(i+1) - ys(i));
    end

    % 1cm縺斐→縺ｮ轤ｹ繧定ｻ碁％縺ｨ縺励※菫晏ｭ?
    path_length_max = 2000;
    path = zeros(path_length_max, 2);
    path(1, 1) = via(1, 1);     %x
    path(1, 2) = via(1, 2);     %y
    dist_buf = 0;
    k = 1;
    for i = 1 : n_spline_point
        dist_buf = dist_buf + dist(i, 1);
        if dist_buf > 0.0099
            k = k + 1;
            path(k, 1) = xs(i);
            path(k, 2) = ys(i);
            dist_buf = 0;
        end
    end

    path_length = k;
    % trajectory縺ｮ辟｡鬧?縺ｪ隕∫ｴ?繧貞炎髯､
    for i = path_length_max : -1 : path_length + 1
        path(i, :) = [];
    end
end

% 繧ｯ繝ｭ繧ｽ繧､繝画峇邱夂沿MakeP
function [path, path_length] = MakePathClothoid(via)
    % 繧ｯ繝ｭ繧ｽ繧､繝峨ｒ險育ｮ?
    S = ClothoidSplineG2();
    SL = S.buildP2( via(:,1), via(:,2));
    point=SL.out(5000);
    xs=point(1,:);
    ys=point(2,:);

    %figure(1);
    %plot(xs,ys,'r');

    % 轤ｹ髢薙?ｮ霍晞屬繧定ｨ育ｮ?
    [~, n_spline_point] = size(xs);
    dist = zeros(n_spline_point, 1);
    for i = 1 : n_spline_point - 1
        dist(i, 1) = hypot(xs(i+1) - xs(i), ys(i+1) - ys(i));
    end

    % 1cm縺斐→縺ｮ轤ｹ繧定ｻ碁％縺ｨ縺励※菫晏ｭ?
    path_length_max = 2000;
    path = zeros(path_length_max, 2);
    path(1, 1) = via(1, 1);     %x
    path(1, 2) = via(1, 2);     %y
    dist_buf = 0;
    k = 1;
    for i = 1 : n_spline_point
        dist_buf = dist_buf + dist(i, 1);
        if dist_buf > 0.0099
            k = k + 1;
            path(k, 1) = xs(i);
            path(k, 2) = ys(i);
            dist_buf = 0;
        end
    end

    path_length = k;
    % trajectory縺ｮ辟｡鬧?縺ｪ隕∫ｴ?繧貞炎髯､
    for i = path_length_max : -1 : path_length + 1
        path(i, :) = [];
    end
end

% 経路から曲率マップを生成
function [curvature_map] = MakeCurvatureMap(path, path_length)
    section = 0.1;      %曲率を求める区画の幅(m)
    curvature_map = zeros(path_length, 1);  %曲率マップゼロクリア
    for i = ceil(section * 100 / 2) + 3 : path_length - (floor(section * 100 / 2) + 1)
        fdiff_x = path(i + round(section * 100 / 2) + 1, 1) - path(i + round(section * 100 / 2) - 1, 1);
        fdiff_y = path(i + round(section * 100 / 2) + 1, 2) - path(i + round(section * 100 / 2) - 1, 2);
        if (fdiff_y >= 0)
            ftheta = atan2(fdiff_y, fdiff_x);
        elseif (fdiff_y < 0)
            ftheta = 2 * pi + atan2(fdiff_y, fdiff_x);
        end
        
        bdiff_x = path(i - round(section * 100 / 2) + 1, 1) - path(i - round(section * 100 / 2) - 1, 1);
        bdiff_y = path(i - round(section * 100 / 2) + 1, 2) - path(i - round(section * 100 / 2) - 1, 2);
        if (bdiff_y >= 0)
            btheta = atan2(bdiff_y, bdiff_x);
        elseif (bdiff_y < 0)
            btheta = 2 * pi + atan2(bdiff_y, bdiff_x);
        end
        
        % 角度差を求める
        theta_diff = abs(ftheta - btheta);
        if (theta_diff > pi)
            theta_diff = 2 * pi - theta_diff;
        end
        
        %曲率算出
        curvature_map(i, 1) = theta_diff / section;
    end
end

% 曲率マップから速度マップを生成
function [velocity_map] = MakeVelocityMap(path_length, curvature_map, vel_max, lat_acc_max, acc_max)
    % 曲率マップから制限最大速度を決定
    velocity_map = zeros(path_length, 1);   %速度マップゼロクリア
    for i = 1 : path_length
        if (curvature_map(i, 1) < 0.3)
            velocity_map(i) = vel_max;
        else
            velocity_map(i) = sqrt(lat_acc_max / curvature_map(i, 1));
            if (velocity_map(i) > vel_max)
                velocity_map(i) = vel_max;
            elseif (velocity_map(i) < 0.5)
                velocity_map(i) = 0.5;
            end
        end
    end

    % 台形加減速生成
    velocity_map(1) = 0;
    velocity_map(path_length) = 0;
    for i = 1 : (path_length - 1)
        velocity_map(i + 1, 1) = min(velocity_map(i + 1), sqrt((velocity_map(i)).^2 + 2 * acc_max * 0.01));
    end
    for i = (path_length - 1) : -1 : 1
        velocity_map(i) = min(velocity_map(i), sqrt((velocity_map(i + 1)).^2 + 2 * acc_max * 0.01));
    end
end

% 回転角速度マップ生成（未完成）
function [angvel_map] = MakeAngVelMap(angvel_max, angacc_max)
    angvel_map = ones(100, 1) * angvel_max;
    angvel_map(1) = 0;
    angvel_map(size(angvel_map)) = 0;
    for i = 1 : size(angvel_map) - 1
        angvel_map(i+1) = min(angvel_map(i+1), sqrt((angvel_map(i)).^2 + 2 * angacc_max * 0.01));
    end
    for i = size(angvel_map) - 1 : -1 : 1
        angvel_map(i) = min(angvel_map(i), sqrt((angvel_map(i+1)).^2 + 2 * angacc_max * 0.01));
    end
    angvel_map = angvel_map * angle_start_end(3);
end

% Pure Pursuit
function [actual_path, angle_ref, expected_time_ms] = PurePursuit(path, path_length, velocity_map, angvel_map, start_angle, CTRL_CYCLE, skid_steer_mode)
    pos = [path(1, 1) path(1, 2) start_angle];  %自己位置・角度
    actual_path = [path(1, 1), path(1, 2), start_angle];
    angle_ref = [start_angle; start_angle;];
    time_ms = 1;
    nearest_index = 1;
    while nearest_index < path_length - 1
        prev_nearest_index = nearest_index;
        % 前回のnearest_indexの前後10cmから次のnearest_indexを探索
        min_length = 1;
        %[path_length, nearest_index]
        for i = max(nearest_index - 10, 1) : min(nearest_index + 10, path_length)
            if (hypot((pos(1) - path(i, 1)).^2, (pos(2) - path(i, 2)).^2) < min_length)
                nearest_index = max(i, 2);
                min_length = hypot((pos(1) - path(i, 1)).^2, (pos(2) - path(i, 2)).^2);
            end
        end
        % 速度に応じてreference_indexを遠くにする
        reference_index = min(nearest_index + round(velocity_map(nearest_index) * 10) + 1, path_length);
        
        % reference_indexと現在位置のズレを計算
        diff_x = path(reference_index, 1) - pos(1);
        diff_y = path(reference_index, 2) - pos(2);
        
        % 現在位置を更新
        pos(1) = pos(1) + velocity_map(nearest_index) * diff_x / hypot(diff_x, diff_y) * CTRL_CYCLE;
        pos(2) = pos(2) + velocity_map(nearest_index) * diff_y / hypot(diff_x, diff_y) * CTRL_CYCLE;
        pos(3) = pos(3) + angvel_map(round(abs((pos(3) - start_angle)) * 100) + 2) * CTRL_CYCLE;
        % ???
        if nearest_index > (prev_nearest_index + 1)
            for i = prev_nearest_index : nearest_index
                angle_ref(i) = pos(3);
            end
        else
            angle_ref(nearest_index) = pos(3);
        end
        
        % 時間軸で保存
        actual_path(time_ms, 1) = pos(1);
        actual_path(time_ms, 2) = pos(2);
        actual_path(time_ms, 3) = pos(3);
        for i = 1 : CTRL_CYCLE * 1000 - 1   %制御周期によってはtime_msが飛び飛びになるのでその間の配列を埋める
            actual_path(time_ms + i, 1) = pos(1);
            actual_path(time_ms + i, 2) = pos(2);
            actual_path(time_ms + i, 3) = pos(3);
        end
        time_ms = time_ms + CTRL_CYCLE * 1000;  %時間更新
    end
    
    % 予想タイム算出
    [expected_time_ms, ~] = size(actual_path);
    expected_time_s = expected_time_ms / 1000;
    fprintf("予想タイム = %f秒", expected_time_s);
end

% 実時間でGifファイルにして保存
function [] = MakeGif(path_num, actual_path, expected_time_ms, MR1_LENGTH, MR1_WIDTH)
    %描画
    fig = figure(5);
    for i = 1 : 100 : expected_time_ms
        x = [actual_path(i,1)-(MR1_WIDTH/2) actual_path(i,1)+(MR1_WIDTH/2) actual_path(i,1)+(MR1_WIDTH/2) actual_path(i,1)-(MR1_WIDTH/2)];
        y = [actual_path(i,2)-(MR1_LENGTH/2) actual_path(i,2)-(MR1_LENGTH/2) actual_path(i,2)+(MR1_LENGTH/2) actual_path(i,2)+(MR1_LENGTH/2)];
        mr1_rectangle = polyshape(x, y);
        mr1_rotate = rotate(mr1_rectangle, actual_path(i, 3) * 180 / pi, [actual_path(i,1) actual_path(i,2)]);
        plot(mr1_rotate, 'FaceColor', 'black', 'FaceAlpha', 0.01)
        drawnow;
        frame = getframe(fig);
        im{i} = frame2im(frame);
    end

    % gif保存
    filename = sprintf('Animation%d.gif', path_num);
    for i = 1 : 100 : expected_time_ms
        [A,map] = rgb2ind(im{i},256);
        if i == 1
            imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0.1);
        else
            imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.1);
        end
    end
end